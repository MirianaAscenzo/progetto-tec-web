SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema grigliatina
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema grigliatina
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `grigliatina` DEFAULT CHARACTER SET utf8 ;
USE `grigliatina` ;

-- -----------------------------------------------------
-- Table `grigliatina`.`Sellers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grigliatina`.`Sellers` ;

CREATE TABLE IF NOT EXISTS `grigliatina`.`Sellers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `mail` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `mail_UNIQUE` (`mail` ASC),
  UNIQUE INDEX `phone_UNIQUE` (`phone` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grigliatina`.`Categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grigliatina`.`Categories` ;

CREATE TABLE IF NOT EXISTS `grigliatina`.`Categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grigliatina`.`Listings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grigliatina`.`Listings` ;

CREATE TABLE IF NOT EXISTS `grigliatina`.`Listings` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` TEXT NOT NULL,
  `price` DOUBLE UNSIGNED NOT NULL,
  `nSales` INT UNSIGNED NOT NULL DEFAULT 0,
  `nAvailable` INT UNSIGNED NOT NULL DEFAULT 0,
  `sellerId` INT NULL,
  `categoryId` INT NOT NULL,
  `image` VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `categoryId_idx` (`categoryId` ASC),
  INDEX `sellerId_idx` (`sellerId` ASC),
  CONSTRAINT `listing_seller`
    FOREIGN KEY (`sellerId`)
    REFERENCES `grigliatina`.`Sellers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `listing_category`
    FOREIGN KEY (`categoryId`)
    REFERENCES `grigliatina`.`Categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grigliatina`.`Buyers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grigliatina`.`Buyers` ;

CREATE TABLE IF NOT EXISTS `grigliatina`.`Buyers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `birthDate` DATE NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `mail` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `phone_UNIQUE` (`phone` ASC),
  UNIQUE INDEX `mail_UNIQUE` (`mail` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grigliatina`.`Orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grigliatina`.`Orders` ;

CREATE TABLE IF NOT EXISTS `grigliatina`.`Orders` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL DEFAULT DATE(NOW()),
  `address` VARCHAR(45) NOT NULL,
  `buyerId` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `buyerId_idx` (`buyerId` ASC),
  CONSTRAINT `order_buyer`
    FOREIGN KEY (`buyerId`)
    REFERENCES `grigliatina`.`Buyers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grigliatina`.`Inclusions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grigliatina`.`Inclusions` ;

CREATE TABLE IF NOT EXISTS `grigliatina`.`Inclusions` (
  `listingId` INT NOT NULL,
  `orderId` INT NOT NULL,
  `quantity` INT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`listingId`, `orderId`),
  INDEX `orderId_idx` (`orderId` ASC),
  CONSTRAINT `inclusion_listing`
    FOREIGN KEY (`listingId`)
    REFERENCES `grigliatina`.`Listings` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `inclusion_order`
    FOREIGN KEY (`orderId`)
    REFERENCES `grigliatina`.`Orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grigliatina`.`ShoppingCartInclusions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grigliatina`.`ShoppingCartInclusions` ;

CREATE TABLE IF NOT EXISTS `grigliatina`.`ShoppingCartInclusions` (
  `listingId` INT NOT NULL,
  `buyerId` INT NOT NULL,
  `quantity` INT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`listingId`, `buyerId`),
  INDEX `buyerId_idx` (`buyerId` ASC),
  CONSTRAINT `scInclusion_listing`
    FOREIGN KEY (`listingId`)
    REFERENCES `grigliatina`.`Listings` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `scInclusion_buyer`
    FOREIGN KEY (`buyerId`)
    REFERENCES `grigliatina`.`Buyers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grigliatina`.`BuyerNotifications`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grigliatina`.`BuyerNotifications` ;

CREATE TABLE IF NOT EXISTS `grigliatina`.`BuyerNotifications` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `text` TEXT NOT NULL,
  `buyerId` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `buyerId_idx` (`buyerId` ASC),
  CONSTRAINT `buyerNotifications_buyer`
    FOREIGN KEY (`buyerId`)
    REFERENCES `grigliatina`.`Buyers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grigliatina`.`SellerNotification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grigliatina`.`SellerNotifications` ;

CREATE TABLE IF NOT EXISTS `grigliatina`.`SellerNotifications` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `text` TEXT NOT NULL,
  `sellerId` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `sellerId_idx` (`sellerId` ASC),
  CONSTRAINT `sellerNotifications_seller`
    FOREIGN KEY (`sellerId`)
    REFERENCES `grigliatina`.`Sellers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

INSERT INTO categories(id, name) VALUES
(1, 'Altro'),
(2, 'Statuette'),
(3, 'Prodotti per grigliate'),
(4, 'Giardinaggio'),
(5, 'Mobili da esterno'),
(6, 'Illuminazione'),
(7, 'Idraulica');
