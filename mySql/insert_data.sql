INSERT INTO sellers(id, mail, password, name, address, phone) values
(1, 'prova@gmail.com', '123', 'prova', 'via prova 1 Imola', '123456789'),
(2, 'second@gmail.com', '123', 'second', 'via second 1 Imola', '223456789'),
(3, 'third@gmail.com', '123', 'third', 'via third 1 Imola', '333456789');

INSERT INTO listings(id, categoryId, sellerId, name, description, price, nSales, nAvailable, image) values
(1, 2, 1, 'nano', 'statuetta di un nano da giardino', 13.40, 10, 5, "listing_img1.jpg"),
(2, 4, 1, 'vaso tondo', 'un vaso tondo di diametro 30cm', 9.99, 100, 5, "listing_img2.jpg"),
(3, 4, 1, 'fertilizzante', '1kg di fertilizzante naturale', 6, 80, 0, "listing_img3.jpg"),
(4, 4, 1, 'semi di geraneo', 'bustina con 10 semi di geraneo', 7.89, 40, 5, "listing_img4.jpg"),
(5, 5, 1, 'sedia', 'una sedia bianca in plastia', 4.23, 100, 0, "listing_img5.jpg"),
(6, 5, 1, 'tavolo', 'tavolo in legno da giardino', 36.87, 20, 0, "listing_img6.jpg"),
(7, 5, 1, 'gazebo', 'un gazebo da montare per tutta la famiglia', 4999.99, 0, 1, "listing_img7.jpeg");

INSERT INTO buyers(id, mail, password, name, surname, phone, birthDate) VALUES
(1, 'buyer1@gmail.com', '123', 'buyer1', 'asd', '123456789', '2000-01-01'),
(2, 'buyer2@gmail.com', '1234', 'buyer2', 'asd', '223456789', '2000-01-01'),
(3, 'buyer3@gmail.com', '123', 'buyer3', 'asd', '333456789', '2000-01-01'),
(4, 'buyer4@gmail.com', '123', 'buyer4', 'asd', '444456789', '2000-01-01');

INSERT INTO shoppingcartinclusions(listingId, buyerId, quantity) VALUES
(1, 1, 10),
(7, 1, 1),
(4, 2, 2),
(2, 2, 2);

INSERT INTO orders(id, buyerId, address, date) VALUES
(1, 1, 'via buyer1 Imola', '2020-01-01');

INSERT INTO inclusions(listingId, orderId, quantity) VALUES
(1, 1, 3),
(7, 1, 1);
