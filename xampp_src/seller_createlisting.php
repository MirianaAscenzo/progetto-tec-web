<?php
  require_once "./utils/bootstrap.php";

  //check if the session has a user logged in
  checkSellerLogged();

  if(isset($_POST["submit"])) {
      if(!elaborateNewListingRequest($errorMessage, $dbh)){
          $templateParams["errorMessage"] = $errorMessage;
      } else {
          redirect("seller_home.php");
      }
  }

  //base template params
  $templateParams["css"] = array("base_style.css", "user_base_style.css", "background_style.css", "error_style.css");
  $templateParams["js"] = array("jquery-1.11.3.min.js", "user_base.js");
  $templateParams["title"] = "Grigliatina.it - Crea articolo";
  //section template params
  $templateParams["sectionTemplate"] = "seller_template.php";
  $templateParams["seller"] = $_SESSION["seller"];
  $templateParams["nNotifications"] = $dbh->getNSellerNotifications($_SESSION["seller"]["id"]);
  //page template params
  $templateParams["pageTemplate"] = "seller_createlisting_page.php";
  $templateParams["categories"] = $dbh->getCategories();

  require("./templates/base_template.php");
?>
