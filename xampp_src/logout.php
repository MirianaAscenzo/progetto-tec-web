<?php
    require_once "./utils/bootstrap.php";

    if(isset($_SESSION["buyer"])) {
        unset($_SESSION["buyer"]);
    }
    if(isset($_SESSION["seller"])) {
        unset($_SESSION["seller"]);
    }
    redirect("login.php");
 ?>
