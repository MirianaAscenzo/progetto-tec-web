<header>
    <h2>Login</h2>
</header>
<?php if(isset($templateParams["errorMessage"])) {
    echo "<p>".$templateParams["errorMessage"]."</p>";
}
?>
<form action="" class="col-6 mx-auto" method="POST">

    <div class="container-fluid">
        <ul>
            <li>
                <div class="row">
                    <div class="col">
                        <label for="usertype">Sei un:</label>
                        <select name="usertype" id="usertype" class="btn btn-outline-secondary">
                            <option value="buyer" <?php if(!isset($templateParams["usertype"]) || (isset($templateParams["usertype"]) && $templateParams["usertype"] = "buyer")) {echo "selected=\"selected\"";} ?>>Cliente</option>
                            <option value="seller" <?php if(isset($templateParams["usertype"]) && $templateParams["usertype"] = "seller") {echo "selected=\"selected\"";} ?>>Venditore</option>
                        </select>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col">
                        <label for="mail">Email:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <input type="text" class="form-control" id="mail" name="mail" autocomplete="mail" required <?php if(isset($templateParams["mail"])) {echo 'value = "'.$templateParams["mail"].'"';} ?>/>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col">
                        <label for="mail">Password:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <input type="password" class="form-control" id="password" name="password" autocomplete="current-password" required/>
                    </div>
                    <div class="col-md-1 d-flex justify-content-center">
                        <button type="button"><img src="icons/eye.svg" alt="visibilità password" title="imposta la visibilità della password"></button>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <input type="submit" class="btn btn-dark" name="submit" value="Login" />
                    </div>
                </div>
            </li>
        </ul>
    </div>

</form>
<nav class="col-4 mx-auto">
    <span>non hai ancora un account?</span>
    <ul>
        <li><a href="signup_buyer.php">crea nuovo account come cliente</a></li>
        <li><a href="signup_seller.php">crea nuovo account come venditore</a></li>
    </ul>
</nav>
