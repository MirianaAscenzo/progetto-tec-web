<header>
    <h2>Registrazione cliente</h2>
</header>
<?php if(isset($templateParams["errorMessage"])) {
    echo "<p>".$templateParams["errorMessage"]."</p>";
}
?>
<form action="" class="col-6 mx-auto" method="POST">

    <div class="container-fluid">

        <ul>
            <li>
                <div class="row">
                    <div class="col">
                        <label for="name">Nome:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <input type="text" class="form-control" id="name" name="name" required <?php if(isset($templateParams["name"])) {echo 'value = "'.$templateParams["name"].'"';} ?>/>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col">
                        <label for="surname">Cognome:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <input type="text" class="form-control" id="surname" name="surname" required <?php if(isset($templateParams["surname"])) {echo 'value = "'.$templateParams["surname"].'"';} ?> />
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col">
                        <label for="birthDate">Data di Nascita:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <input type="date" class="form-control" id="birthDate" name="birthDate" min="1900-01-01" max="2021-01-01" required <?php if(isset($templateParams["birthDate"])) {echo 'value = "'.$templateParams["birthDate"].'"';} ?> />
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col">
                        <label for="phone">Telefono:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <input type="number" class="form-control" id="phone" name="phone" required <?php if(isset($templateParams["phone"])) {echo 'value = "'.$templateParams["phone"].'"';} ?>/>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col">
                        <label for="mail">Email:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <input type="email" class="form-control" id="mail" name="mail" required <?php if(isset($templateParams["mail"])) {echo 'value = "'.$templateParams["mail"].'"';} ?>/>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col">
                        <label for="password">Password:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <input type="password" class="form-control" id="password" name="password" value="" autocomplete="new-password" required/>
                    </div>
                    <div class="col-md-1 d-flex justify-content-center">
                        <button type="button"><img src="icons/eye.svg" alt="visibilità password" title="imposta la visibilità della password"></button>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col">
                        <label for="repeat">Ripeti password:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <input type="password" class="form-control" id="repeat" name="repeat" value="" autocomplete="new-password" required />
                    </div>
                    <div class="col-md-1 d-flex justify-content-center">
                        <button type="button"><img src="icons/eye.svg" alt="visibilità password" title="imposta la visibilità della password"></button>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <input type="submit" class="btn btn-dark" name="submit" value="Registrati" />
                    </div>
                </div>
            </li>
        </ul>

    </div>
</form>
