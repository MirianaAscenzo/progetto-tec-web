<header><h2>Ricerca</h2></header>
<section>
    <header><h3>Parametri</h3></header>
    <div class="col-8 mx-auto">
        <ul>
            <li>
                <label for="keyword">Inserisci Keyword:</label>
                <input type="search" id="keyword" name="keyword" class="form-control col-xs-1 text-center"/>
            </li>
            <li>
                <h4>Filtri</h4>
                <button type="button">
                    <img src="icons/caret-down-fill.svg" alt="pulsante di Espansione Filtri" title="Espandi i Filtri">
                </button>
                <fieldset>
                    <ul>
                        <li>
                            <label for="category">Categoria:</label>
                            <select name="category" id="category" class="btn btn-outline-secondary">
                                <option value="all" selected="selected">Tutte le categorie</option>
                                <?php foreach ($templateParams["categories"] as $category): ?>
                                <option value="<?php echo $category["id"] ?>"><?php echo $category["name"] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </li>
                        <li>
                            <span>Range di prezzo:</span>
                            <div class="container-fluid row">
                                <div class="col-sm-6">
                                    <label for="minprice">Min:</label>
                                    <input type="number" name="min" min="0" id="min" title="minprice" class="form-control col-xs-1 text-center"/>
                                </div>
                                <div class="col-sm-6">
                                    <label for="maxprice">Max:</label>
                                    <input type="number" name="max" min="0" id="max" title="maxprice" class="form-control col-xs-1 text-center"/>
                                </div>
                            </div>
                        </li>
                    </ul>
                </fieldset>
            </li>
            <li>
                <button type="submit" class="btn btn-dark">cerca</button>
            </li>
        </ul>
    </div>
</section>
