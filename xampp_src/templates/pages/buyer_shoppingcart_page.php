<header>
    <h2>Carrello</h2>
</header>
<section>
    <header><h3>Articoli</h3></header>
    <div class="container-fluid row">
        <?php foreach ($templateParams["listings"] as $listing): ?>
            <div class="col-md-6">
                <article id="<?php echo $listing["id"]; ?>">
                    <div class="container-fluid row">
                        <div class="col-sm-5 image-column">
                            <img src="<?php echo LISTIMG_DIR.$listing["image"]; ?>" alt ="foto di <?php echo $listing["name"]; ?>" />
                        </div>
                        <div class="col-sm-7 align-self-center">
                            <ul>
                                <li><?php echo $listing["name"]; ?></li>
                                <li>quantità: <span><?php echo $listing["quantity"]; ?></span></li>
                                <li>
                                    <input type="number" name="number" min="1" title="quantity" class="col-4" value="<?php echo $listing["quantity"]; ?>" />
                                    <button type="button" class="btn btn-dark">Cambia Quantità</button>
                                </li>
                                <li>prezzo complessivo: <span><?php echo $listing["quantity"] * $listing["price"];?>€</span></li>
                            </ul>
                            <button type="button">Rimuovi Articolo</button>
                        </div>
                    </div>
                    <footer>
                        <?php if (isset($listing["errorMessage"])) {
                            echo "<p>".$listing["errorMessage"]."</p>";
                        } ?>
                    </footer>
                </article>
            </div>
        <?php endforeach; ?>
    </div>

    <footer><p>Totale: <?php echo $templateParams["total"]; ?>€</p></footer>
</section>
<section>
    <header><h3>Informazioni di Consegna</h3></header>
    <form action="" class="col-8 mx-auto" method="POST">
        <div class="container-fluid">
            <ul>
                <h4>Consegna a:</h4>
                <li>
                    <div class="row">
                        <div class="col">
                            <label for="city">Città:</label>
                        </div>
                        <div class="col-md-12">
                        <input type="text" id="city" name="city" class="form-control" required/>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col">
                            <label for="address">Indirizzo:</label>
                        </div>
                        <div class="col-md-12">
                            <input type="text" id="address" name="address" class="form-control" required />
                        </div>
                    </div>
                </li>
                <h4>Informazioni carta di credito:</h4>
                <li>
                    <div class="row">
                        <div class="col">
                            <label for="card">Numero della Carta di Credito:</label>
                        </div>
                        <div class="col-md-12">
                            <input type="number" id="card" name="card" class="form-control" required />
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col">
                            <label for="code">Codice di Sicurezza:</label>
                        </div>
                        <div class="col-md-12">
                            <input type="number" id="code" name="code" class="form-control" required />
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col d-flex justify-content-center">
                            <button id="payment" type="button" class="btn btn-dark">Procedi col Pagamento</button>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <footer>
            <p id="err"><?php if(isset($templateParams["errorMessage"])){
                echo $templateParams["errorMessage"];
            } ?></p>
        </footer>
    </form>
</section>
