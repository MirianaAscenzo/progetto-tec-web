<header>
    <h2>I miei Ordini</h2>
</header>
<section>
    <?php foreach ($templateParams["orders"] as $order): ?>
    <article id="<?php echo $order["id"]; ?>">
        <header>
            <h3>Codice ordine:<?php echo $order["id"]; ?></h3>
        </header>
        <ul>
            <li>Indirizzo: <span><?php echo $order["address"] ?></span></li>
            <li>Data: <span><?php echo $order["date"] ?></span></li>
            <li>Totale: <span><?php echo $order["total"] ?></span></li>
        </ul>
        <section>
            <span>Articoli Comprati</span>
            <ul>
                <?php foreach ($order["products"] as $product): ?>
                <li><?php echo $product["name"] ?> - quantità: <span><?php echo $product["quantity"] ?></span></li>
                <?php endforeach; ?>
            </ul>
        </section>
    </article>
    <?php endforeach; ?>
</section>