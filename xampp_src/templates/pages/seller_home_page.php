<header>
    <h2>Articoli</h2>
</header>
<div class="col-md-12 d-flex justify-content-center">
    <a href="seller_createlisting.php" class="btn btn-dark">Crea nuovo Articolo</a>
</div>
<section>
    <div class="container-fluid">
        <div class="row">
            <?php foreach($templateParams["listings"] as $listing): ?>
                <div class="col-md-6">
                    <article id="<?php echo $listing["id"]; ?>">
                        <header>
                            <h3><?php echo $listing["name"]; ?></h3>
                        </header>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-5 image-column">
                                    <div>
                                        <img src="<?php echo LISTIMG_DIR.$listing["image"]; ?>" alt="foto di <?php echo $listing["name"]; ?>" />
                                    </div>
                                </div>
                                <div class="col-sm-7 align-self-center">
                                    <ul>
                                        <li>Prezzo: <span><?php echo $listing["price"]."€"; ?></span></li>
                                        <li>Categoria: <span><?php echo $listing["categoryName"]; ?></span></li>
                                        <li>Quantità: <span><?php echo $listing["nAvailable"]; ?></span></li>
                                        <li>
                                            <input type="number" name="quantity" min="1" value ="1" title="quantity" class="col-4"/>
                                            <button type="button" class="col-6 btn btn-dark" >Rifornisci</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <section>
                                        <span>Descrizione:</span><button type="button">Leggi tutto</button>
                                        <p><?php echo $listing["description"]; ?></p>
                                    </section>
                                </div>
                            </div>
                        </div>
                        <footer>
                            <button type="button">Rimuovi Articolo</button>
                        </footer>
                    </article>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>