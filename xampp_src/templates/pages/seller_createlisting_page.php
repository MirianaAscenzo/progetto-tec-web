<header>
    <h2>Creazione Nuovo Articolo</h2>
</header>
<section>
    <header>
        <h3>Inserisci nuovo articolo</h3>
    </header>
    <p><?php if(isset($templateParams["errorMessage"])) {
        echo $templateParams["errorMessage"];
    } ?></p>
    <form action="" class="col-8 mx-auto" method="POST" enctype="multipart/form-data" >
        <div class="container-fluid">
            <ul>
                <li>
                    <div class="row">
                        <div class="col">
                            <label for="name">Inserisci nome articolo:</label>
                        </div>
                        <div class="col-md-12">
                            <input type="text" id="name" name="name" class="form-control"/>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col">
                            <label for="price">Inserisci prezzo articolo:</label>
                        </div>
                        <div class="col-md-12">
                            <input type="number" id="price" name="price" step="0.01" class="form-control"/>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col">
                            <label for="category">Inserisci Categoria:</label>
                        </div>
                        <div class="col-md-12">
                            <select name="category" id="category" class="btn btn-light">
                                <?php foreach ($templateParams["categories"] as $category): ?>
                                <option value="<?php echo $category["id"]; ?>"><?php echo $category["name"]; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col">
                            <label for="description">Inserisci descrizione articolo:</label>
                        </div>
                        <div class="col-md-12">
                            <textarea name="description" id="description" cols="30" rows="10" class="form-control" ></textarea>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col">
                            <label for="image">Inserisci immagine articolo:</label>
                        </div>
                        <div class="col-md-12">
                            <input type="file" id="image" name="image" accept="image/png, image/jpeg, image/jpg" class="form-control"/>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col d-flex justify-content-center">
                            <input type="submit" name="submit" class="btn btn-dark" value="Pubblica"/>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </form>
</section>
