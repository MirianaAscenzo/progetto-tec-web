<header>
    <h2>Notifiche</h2>
</header>
<section>
    <?php foreach ($templateParams["notifications"] as $notification): ?>
        <article id="<?php echo $notification["id"]; ?>">
            <p><?php echo $notification["text"]; ?></p>
            <footer>
                <button type="button" name="button" class="btn btn-dark">Segna come letto</button>
            </footer>
        </article>
    <?php endforeach; ?>
</section>
