<header>
    <h2>Home</h2>
</header>
<section>
    <p>Benvenuti su Grigliatina.it, la piattaforma di acquisti online per articoli da giardino!</p>
    <?php if(isset($templateParams["category"])) {
        echo "<p>Ecco alcuni articoli dalla categoria <span>"
             .$templateParams["category"]
             ."</span> che potrebbero interessarti:</p>";
    }
    ?>
</section>
<?php foreach ($templateParams["home_categories"] as $categoryName => $listings): ?>
<article>
    <header>
        <h3><?php echo $categoryName; ?></h3>
    </header>
    <div class="container-fluid row">
        <?php foreach ($listings as $id => $listing): ?>
            <div class="col-md-6">
                <article id="<?php echo $listing["id"]; ?>">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6 image-column">
                                <img src="<?php echo LISTIMG_DIR.$listing["image"]; ?>" alt ="foto di <?php echo $listing["name"]; ?>" />
                            </div>
                            <div class="col-sm-6 align-self-center">
                                <ul>
                                    <li><?php echo $listing["name"]; ?></li>
                                    <li>Prezzo: <span><?php echo $listing["price"]; ?>€</span></li>
                                </ul>
                                <button type="button" class="col-10 btn btn-dark">Aggiungi al Carrello </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <section>
                                    <span>Descrizione</span><button type="button">Leggi tutto</button>
                                    <p><?php echo $listing["description"]; ?></p>
                                </section>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        <?php endforeach; ?>
    </div>
</article>
<?php endforeach; ?>
