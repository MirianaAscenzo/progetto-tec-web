<header>
    <h2>Registrazione venditore</h2>
</header>
<?php if(isset($templateParams["errorMessage"])) {
    echo "<p>".$templateParams["errorMessage"]."</p>";
}
?>
<form action="" class="col-6 mx-auto" method="POST">
    <div class="container-fluid">
    
    <ul>
        <li>
            <div class="row">
                <div class="col">
                    <label for="name">Nome Negozio:</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11">
                    <input type="text" class="form-control" id="name" name="name" required <?php if(isset($templateParams["name"])) {echo 'value = "'.$templateParams["name"].'"';} ?> />
                </div>
            </div>
        </li>
        <li>
            <div class="row">
                <div class="col">
                <label for="address">Indirizzo:</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11">
                    <input type="text" class="form-control" id="address" name="address" required <?php if(isset($templateParams["address"])) {echo 'value = "'.$templateParams["address"].'"';} ?> />
                </div>
            </div>
        </li>
        <li>
            <div class="row">
                <div class="col">
                    <label for="mail">Email:</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11">
                    <input type="email" class="form-control" id="mail" name="mail" required <?php if(isset($templateParams["mail"])) {echo 'value = "'.$templateParams["mail"].'"';} ?>/>
                </div>
            </div>
        </li>
        <li>
            <div class="row">
                <div class="col">
                    <label for="password">Password:</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11">
                    <input type="password" class="form-control" id="password" name="password" value="" autocomplete="new-password" required />
                </div>
                <div class="col-md-1 d-flex justify-content-center">
                    <button type="button"><img src="icons/eye.svg" alt="visibilità password" title="imposta la visibilità della password" ></button>
                </div>
            </div>
        </li>
        <li>
            <div class="row">
                <div class="col">
                    <label for="repeat">Ripeti password:</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11">
                    <input type="password" class="form-control" id="repeat" name="repeat" value="" autocomplete="new-password" required/>
                </div>
                <div class="col-md-1 d-flex justify-content-center">
                    <button type="button"><img src="icons/eye.svg" alt="visibilità password" title="imposta la visibilità della password"></button>
                </div>
            </div>
        </li>
        <li>
            <div class="row">
                <div class="col d-flex justify-content-center">
                    <input type="submit" class="btn btn-dark" name="submit" value="Registrati" />
                </div>
            </div>
        </li>
    </ul>

    </div>
</form>
