<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="./css/bootstrap.min.css" />
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>

        <?php foreach ($templateParams["js"] as $filename): ?>
        <script src="./js/<?php echo $filename; ?>" type="text/javascript"></script>
        <?php endforeach; ?>

        <?php foreach ($templateParams["css"] as $filename): ?>
        <link rel="stylesheet" href="./css/<?php echo $filename; ?>" >
        <?php endforeach; ?>

        <title><?php echo $templateParams["title"]; ?></title>
    </head>
    <body>
        <?php require("./templates/".$templateParams["sectionTemplate"]); ?>
    </body>
</html>
