<header>
    <h1>Grigliatina.it</h1>
    <nav>
        <ul>
            <li>
                <button type="button"><img src="icons/list.svg" alt="pulsante del Menù" title="Apri il Menù"></button>
            </li>
            <li>
                <a href="seller_notifications.php"><img src="icons/bell.svg" alt="pulsante delle Notifiche" title="Vai alle Notifiche"></a>
                <?php
                    if($templateParams["nNotifications"] > 0) {
                        echo "<span>".$templateParams["nNotifications"]."</span>";
                    }
                 ?>
            </li>
        </ul>
    </nav>
</header>

<aside>
    <ul>
        <li><a href="seller_home.php">I miei Articoli</a></li>
        <li><a href="seller_createlisting.php">Crea Nuovo Articolo</a></li>
        <li><a href="seller_support.php">Supporto</a></li>
        <li>
            <span>Informazioni Utente</span><button type="button">Espandi</button>
            <ul>
                <li><?php echo $templateParams["seller"]["name"]; ?></li>
                <li><?php echo $templateParams["seller"]["address"]; ?></li>
                <li><?php echo $templateParams["seller"]["mail"]; ?></li>
            </ul>
        </li>
        <li><button type="button">Logout</button></li>
    </ul>
</aside>

<main>
    <?php require("./templates/pages/".$templateParams["pageTemplate"]); ?>
</main>

<footer>
    <p>Per tutte le tue esigenze in fatto di giardini!</p>
</footer>
