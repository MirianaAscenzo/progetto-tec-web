<header>
    <h1>Grigliatina.it</h1>
    <nav>
        <ul>
            <li>
                <button type="button"><img src="icons/list.svg" alt="pulsante del Menù" title="Apri il Menù"></button>
            </li>
            <li>
                <a href="buyer_shoppingcart.php"><img src="icons/cart2.svg" alt="pulsante del Carrello" title="Vai al Carrello"></a>
            </li>
            <li>
                <a href="buyer_search.php"><img src="icons/search.svg" alt="pulsante di Ricerca" title="Cerca un Prodotto"></a>
            </li>
            <li>
                <a href="buyer_notifications.php"><img src="icons/bell.svg" alt="pulsante delle Notifiche" title="Vai alle Notifiche"></a>
                <?php
                    if($templateParams["nNotifications"] > 0) {
                        echo "<span>".$templateParams["nNotifications"]."</span>";
                    }
                 ?>
            </li>
        </ul>
    </nav>
</header>

<aside>
    <ul>
        <li><a href="buyer_home.php">Home</a></li>
        <li><a href="buyer_orders.php">I miei ordini</a></li>
        <li><a href="buyer_support.php">Supporto</a></li>
        <li>
            <span>Informazioni Utente</span><button type="button">Espandi</button>
            <ul>
                <li><?php echo $templateParams["buyer"]["name"]; ?></li>
                <li><?php echo $templateParams["buyer"]["surname"]; ?></li>
                <li><?php echo $templateParams["buyer"]["birthDate"]; ?></li>
                <li><?php echo $templateParams["buyer"]["phone"]; ?></li>
                <li><?php echo $templateParams["buyer"]["mail"]; ?></li>
            </ul>
        </li>
        <li><button type="button">Logout</button></li>
    </ul>
</aside>

<main>
    <?php require("./templates/pages/".$templateParams["pageTemplate"]); ?>
</main>

<footer>
    <p>Per tutte le tue esigenze in fatto di giardini!</p>
</footer>
