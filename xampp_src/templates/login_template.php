<header>
    <h1>Grigliatina.it</h1>
</header>
<main>
    <?php require("./templates/pages/".$templateParams["pageTemplate"]); ?>
</main>
<footer>
    <p>Per tutte le tue esigenze in fatto di giardini!</p>
</footer>
