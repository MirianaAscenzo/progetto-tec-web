<?php
    //add listing, remove listing or set listing quantity

    //use variable addId=* for adding a listing

    //use variable remId=* for removal

    //use variables id=* and quantity=* to set a quantity, and responds with the new price on success

    require_once "./utils/bootstrap.php";

    if(isset($_POST["addId"])) {
        if($dbh->addToCart($_SESSION["buyer"]["id"], $_POST["addId"]) == 1) {
            http_response_code(200);
        }
    } else if(isset($_POST["remId"])) {
        if($dbh->removeFromCart($_SESSION["buyer"]["id"], $_POST["remId"]) == 1) {
            http_response_code(200);
            $listings = $dbh->getShoppingCartListings($_SESSION["buyer"]["id"]);
            $total = 0;
            foreach ($listings as $listing) {
                $total = $total + ($listing["quantity"] * $listing["price"]);
            }
            echo json_encode(array("total" => bcdiv($total, 1, 2)));
        } else {
            http_response_code(400);
        }
    } else if(isset($_POST["id"]) && isset($_POST["quantity"])) {
        if($dbh->setCartListingQuantity($_SESSION["buyer"]["id"], $_POST["id"], $_POST["quantity"]) == 1) {
            http_response_code(200);
            $listings = $dbh->getShoppingCartListings($_SESSION["buyer"]["id"]);
            $total = 0;
            $newprice = 0;
            foreach ($listings as $listing) {
                $total = $total + ($listing["quantity"] * $listing["price"]);
                if($listing["id"] == $_POST["id"]) {
                    $newprice = $_POST["quantity"]*$listing["price"];
                }
            }
            echo json_encode(array("newprice" => bcdiv($newprice, 1, 2), "total" => bcdiv($total, 1, 2)));
        } else {
            http_response_code(400);
        }
    } else {
        http_response_code(400);
    }
    
?> 