<?php
    require_once "./utils/bootstrap.php";

    //check user isn't logged in
    redirectIfLoggedIn();

    if(isset($_SESSION["over"])) {
        unset($_SESSION["over"]);
        $templateParams["errorMessage"] = "La sessione è scaduta, si prega di rifare l'accesso";
    }

    //handle authentication
    if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["mail"])) {
        if($_POST["usertype"] == "buyer") {
            $buyer = $dbh->buyerLogin($_POST["mail"], $_POST["password"]);
            if($buyer != false) {
                $_SESSION["buyer"] = $buyer;
                redirect("buyer_home.php");
            }
        }
        if($_POST["usertype"] == "seller") {
            $seller = $dbh->sellerLogin($_POST["mail"], $_POST["password"]);
            if($seller != false) {
                $_SESSION["seller"] = $seller;
                redirect("seller_home.php");
            }
        }
        $templateParams["usertype"] = $_POST["usertype"];
        $templateParams["mail"] = $_POST["mail"];
        $templateParams["errorMessage"] = "Mail o password non validi";
    }

    //base template params
    $templateParams["css"] = array("base_style.css", "login_style.css");
    $templateParams["js"] = array("jquery-1.11.3.min.js", "password_visibility.js");
    $templateParams["title"] = "Grigliatina.it - Login";
    //section template params
    $templateParams["sectionTemplate"] = "login_template.php";
    //page template params
    $templateParams["pageTemplate"] = "login_page.php";

    require("./templates/base_template.php");
 ?>
