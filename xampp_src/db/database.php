<?php
  class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port=3306) {
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
    }

    //returns the seller if login is successful, or false otherwise
    public function sellerLogin($mail, $password) {
        $stmt = $this->db->prepare("SELECT * FROM sellers WHERE mail = ? AND password = ?");
        $stmt->bind_param("ss", $mail, $password);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if(count($result) == 0) {
           return false;
        }
        return $result[0];
    }

    //returns the buyer if login is successful, or false otherwise
    public function buyerLogin($mail, $password) {
        $stmt = $this->db->prepare("SELECT * FROM buyers WHERE mail = ? AND password = ?");
        $stmt->bind_param("ss", $mail, $password);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if(count($result) == 0) {
           return false;
        }
        return $result[0];
    }

    //return 0 if they are ok, 1 if mail is taken and 2 if phone is taken
    public function checkUniqueBuyerData($mail, $phone) {
        $mailstmt = $this->db->prepare("SELECT id FROM buyers WHERE mail = ?");
        $mailstmt->bind_param("s", $mail);
        $mailstmt->execute();
        $mailduplicates = $mailstmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if(count($mailduplicates) > 0) {
            return 1;
        }
        $phonestmt = $this->db->prepare("SELECT id FROM buyers WHERE phone = ?");
        $phonestmt->bind_param("s", $phone);
        $phonestmt->execute();
        $phoneduplicates = $phonestmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if(count($phoneduplicates) > 0) {
            return 2;
        }
        return 0;
    }

    //return 0 if they are ok, 1 if mail is taken and 2 if phone is taken
    public function checkUniqueSellerData($mail, $phone) {
        $mailstmt = $this->db->prepare("SELECT id FROM sellers WHERE mail = ?");
        $mailstmt->bind_param("s", $mail);
        $mailstmt->execute();
        $mailduplicates = $mailstmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if(count($mailduplicates) > 0) {
            return 1;
        }
        $phonestmt = $this->db->prepare("SELECT id FROM buyers WHERE phone = ?");
        $phonestmt->bind_param("s", $phone);
        $phonestmt->execute();
        $phoneduplicates = $phonestmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if(count($phoneduplicates) > 0) {
            return 2;
        }
        return 0;
    }

    //return the 2 most popular listings from 2 random categories
    public function homepageListings() {
        $sql = "SELECT L.*, C.name as categoryName
                FROM listings L JOIN 
                   (SELECT CA.* 
                    FROM categories CA
                    WHERE CA.id != 1 
                    AND (SELECT COUNT(*) 
                         FROM listings LI 
                         WHERE LI.categoryId = CA.id 
                         AND LI.sellerId IS NOT NULL) > 0
                    ORDER BY RAND() LIMIT 1) C
                ON (L.categoryId = C.id)
                WHERE L.sellerId IS NOT NULL
                ORDER BY L.nSales DESC
                LIMIT 2";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->get_result();
        $listings = $result->fetch_all(MYSQLI_ASSOC);
        $categories = array();
        foreach ($listings as $listing) {
            $categories[$listing["categoryName"]][$listing["id"]] = $listing;
        }
        return $categories;
    }

    private function isListingSearched($listing, $nameSubstring) {
        return substr_count(strtolower($listing["name"]), $nameSubstring) >= 1;
    }

    private function filterListingsByName($listings, $nameSubstring) {
        if($nameSubstring == "") {
            return $listings;
        }
        $nameSubstring = strtolower($nameSubstring); 
        $filterFunction = function ($row) use(&$nameSubstring){
            return $this->isListingSearched($row, $nameSubstring);
        };
        return array_filter($listings, $filterFunction, ARRAY_FILTER_USE_BOTH);
    }

    //search listings that match substring and price limits (price limits of 0 are ignored)
    public function searchListings($nameSubstring, $minPrice = 0, $maxPrice = 0) {
        $sql = "SELECT L.*, C.name as categoryName
                FROM listings L, categories C
                WHERE L.categoryId = C.id
                AND (? = 0 OR ? <= L.price) 
                AND (? = 0 OR ? >= L.price) 
                AND L.sellerId IS NOT NULL";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('iiii', $minPrice, $minPrice, $maxPrice, $maxPrice);
        $stmt->execute();
        $listings = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        return $this->filterListingsByName($listings, $nameSubstring);
    }

    //search listings that match substring, category and price limits (price limits of 0 are ignored)
    public function searchListingsByCategory($nameSubstring, $categoryId, $minPrice = 0, $maxPrice = 0) {
        $sql = "SELECT L.*, C.name as categoryName
                FROM listings L, categories C
                WHERE L.categoryId = C.id 
                AND (? = 0 OR ? <= price) 
                AND (? = 0 OR ? >= price) 
                AND (categoryId = ?)  
                AND sellerId IS NOT NULL";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('iiiii', $minPrice, $minPrice, $maxPrice, $maxPrice, $categoryId);
        $stmt->execute();
        $listings = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        return $this->filterListingsByName($listings, $nameSubstring);
    }

    //get listings and quantities of the buyer's shopping cart
    public function getShoppingCartListings($buyerId) {
        $sql = "SELECT L.*, S.quantity
                FROM listings L JOIN shoppingcartinclusions S
                ON (L.id = S.listingId)
                WHERE S.buyerId = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('i', $buyerId);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    //get listingId and availability of unavailable listings in the shopping cart
    public function getUnavailableShoppingCartListings($buyerId) {
        $sql = "SELECT L.id, L.nAvailable
                FROM listings L JOIN shoppingcartinclusions S
                ON (L.id = S.listingId)
                WHERE S.buyerId = ?
                AND L.nAvailable < S.quantity";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('i', $buyerId);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    private function sendSellerNotification($sellerId, $text) {
        $stmt = $this->db->prepare("INSERT INTO sellernotifications(sellerId, text) VALUES (?, ?)");
        $stmt->bind_param('is', $sellerId, $text);
        $stmt->execute();
        $stmt->close();
    }

    private function sendBuyerNotification($buyerId, $text) {
        $stmt = $this->db->prepare("INSERT INTO buyernotifications(buyerId, text) VALUES (?, ?)");
        $stmt->bind_param('is', $buyerId, $text);
        $stmt->execute();
        $stmt->close();
    }

    private function notifySoldOut($listingBought) {
        $text = "Il tuo articolo ". $listingBought["name"].
        "\n ha esaurito le scorte, aggiungi disponibilità per tale prodotto
        \n dalla pagina dei prodotti";
        $this->sendSellerNotification($listingBought["sellerId"], $text);
    }

    private function notifyShipped($listingBought, $address, $buyerId) {
        $text = "L'articolo ". $listingBought["name"].
                "\n è stato spedito in quantità x". $listingBought["quantity"].
                "\n all'indirizzo ". $address;
        $this->sendBuyerNotification($buyerId, $text);
    }

    //check availabilities and execute purchase, (0: successful, -1: unavailable items, -2: no items in cart, -3: database error)
    public function purchaseShoppingCartListings($buyerId, $address) {
        $shoppingCart = $this->getShoppingCartListings($buyerId);
        if(count($shoppingCart) == 0) {
            return -2;//no items in shopping cart
        }
        if(count($this->getUnavailableShoppingCartListings($buyerId)) > 0) {
            return -1;//unavailable items
        }

        $stmt = $this->db->prepare("UPDATE listings
                                    SET nAvailable = nAvailable - ?, nSales = nSales + ?
                                    WHERE id = ?");

        //subtract availabilities and send soldout notifications
        foreach ($shoppingCart as $listingBought) {
            $stmt->bind_param('iii', $listingBought["quantity"], $listingBought["quantity"], $listingBought["id"]);
            $stmt->execute();
            if($listingBought["nAvailable"] - $listingBought["quantity"] <= 0) {
                $this->notifySoldOut($listingBought);
            }
        }
        $stmt->close();

        //add order tuple
        $addOrderSql = "INSERT INTO orders(buyerId, address) VALUES (?, ?)";
        $stmt = $this->db->prepare($addOrderSql);
        $stmt->bind_param('is', $buyerId, $address);
        $stmt->execute();
        $stmt->close();

        $stmt = $this->db->prepare("INSERT INTO inclusions(listingId, orderId, quantity)
                                    VALUES (?, ?, ?)");

        $orderId = $this->db->query("SELECT last_insert_id()")->fetch_row()[0];

        //add inclusions tuples
        foreach ($shoppingCart as $listingBought) {
            $stmt->bind_param('iii', $listingBought["id"], $orderId, $listingBought["quantity"]);
            $stmt->execute();
        }
        $stmt->close();

        //empty shopping cart
        $stmt = $this->db->prepare("DELETE FROM shoppingcartinclusions
                                     WHERE buyerId = ?");
        $stmt->bind_param('i', $buyerId);
        $stmt->execute();
        $stmt->close();

        //notify customer that items are being shipped
        foreach ($shoppingCart as $listingBought) {
            $this->notifyShipped($listingBought, $address, $buyerId);
        }
        return 0;
    }

    //get orders of a customer and products for each order (key = "products")
    public function getOrders($buyerId) {
      $stmt = $this->db->prepare("SELECT O.*, SUM(I.quantity*L.price) as total
                                  FROM orders O, inclusions I, listings L
                                  WHERE O.id = I.orderId
                                  AND I.listingId = L.id
                                  AND O.buyerId = ?
                                  GROUP BY O.id");
      $stmt->bind_param('i', $buyerId);
      $stmt->execute();
      $orders = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
      $stmt->close();

      $stmt = $this->db->prepare("SELECT L.name, I.quantity
                                  FROM listings L JOIN inclusions I
                                  ON (L.id = I.listingId)
                                  WHERE I.orderId = ?");

      for ($i=0; $i < count($orders); $i++) {
          $stmt->bind_param('i', $orders[$i]["id"]);
          $stmt->execute();
          $products = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
          $orders[$i]["products"] = $products;
      }
      $stmt->close();

      return $orders;
    }

    //get number of current buyer notifications
    public function getNBuyerNotifications($buyerId) {
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM buyernotifications WHERE buyerId = ?");
        $stmt->bind_param('i', $buyerId);
        $stmt->execute();
        $count = $stmt->get_result()->fetch_row()[0];
        $stmt->close();
        return $count;
    }

    //get number of current seller notifications
    public function getNSellerNotifications($sellerId) {
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM sellernotifications WHERE sellerId = ?");
        $stmt->bind_param('i', $sellerId);
        $stmt->execute();
        $count = $stmt->get_result()->fetch_row()[0];
        $stmt->close();
        return $count;
    }

    //get buyer notifications
    public function getBuyerNotifications($buyerId) {
        $stmt = $this->db->prepare("SELECT * FROM buyernotifications WHERE buyerId = ?");
        $stmt->bind_param('i', $buyerId);
        $stmt->execute();
        $notifications = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $notifications;
    }

    //get seller notifications
    public function getSellerNotifications($sellerId) {
        $stmt = $this->db->prepare("SELECT * FROM sellernotifications WHERE sellerId = ?");
        $stmt->bind_param('i', $sellerId);
        $stmt->execute();
        $notifications = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $notifications;
    }

    //add a listing to buyer's to cart with quantity 1
    public function addToCart($buyerId, $listingId) {
        $stmt = $this->db->prepare("INSERT INTO shoppingcartinclusions(buyerId, listingId) VALUES (?, ?)");
        $stmt->bind_param('ii', $buyerId, $listingId);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }

    //set quantity of an item in buyer's shopping cart
    public function setCartListingQuantity($buyerId, $listingId, $quantity) {
        if($quantity < 1 || $quantity > 10) {
            return 0;
        }
        $stmt = $this->db->prepare("UPDATE shoppingcartinclusions
                                    SET quantity = ?
                                    WHERE buyerId = ?
                                    AND listingId = ?");
        $stmt->bind_param('iii', $quantity, $buyerId, $listingId);
        $stmt->execute();
        return $stmt->affected_rows;
    }

    //remove an item from buyer's shopping cart;
    public function removeFromCart($buyerId, $listingId) {
        $stmt = $this->db->prepare("DELETE FROM shoppingcartinclusions
                                    WHERE buyerId = ?
                                    AND listingId = ?");
        $stmt->bind_param('ii', $buyerId, $listingId);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }

    //remove notification from selleNotifications (1: success, 0:failure)
    public function removeSellerNotification($notificationId, $sellerId = null) {
        if($sellerId == null) {
            $sql = "DELETE FROM sellernotifications WHERE id = ?";
        } else {
            $sql = "DELETE FROM sellernotifications WHERE id = ? AND sellerId = ".$sellerId;
        }
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('i', $notificationId);
        $stmt->execute();
        return $stmt->affected_rows;
    }

    //remove notification from buyerNotifications (1: success, 0:failure)
    public function removeBuyerNotification($notificationId, $buyerId = null) {
        if($buyerId == null) {
            $sql = "DELETE FROM buyernotifications WHERE id = ?";
        } else {
            $sql = "DELETE FROM buyernotifications WHERE id = ? AND buyerId = ".$buyerId;
        }
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('i', $notificationId);
        $stmt->execute();
        return $stmt->affected_rows;
    }

    public function getListing($listingId) {
        $stmt = $this->db->prepare("SELECT * FROM listings WHERE id = ?");
        $stmt->bind_param('i', $listingId);
        $stmt->execute();
        $listings = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if(count($listing) == 0) {
            return false;
        }
        return $listings[0];
    }

    //get listings sold from a seller
    public function getListings($sellerId) {
        $stmt = $this->db->prepare("SELECT L.*, C.name as categoryName FROM listings L JOIN categories C ON (L.categoryId = C.id) WHERE L.sellerId = ?");
        $stmt->bind_param('i', $sellerId);
        $stmt->execute();
        $listings = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $listings;
    }

    //add availability on listing (1: success, 0:failure)
    public function addAvailability($listingId, $addedQuantity, $sellerId = null) {
        if($sellerId == null) {
            $sql = "UPDATE listings SET nAvailable = nAvailable + ? WHERE id = ?";
        } else {
            $sql = "UPDATE listings SET nAvailable = nAvailable + ? WHERE id = ? AND sellerId = ".$sellerId;
        }
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('ii', $addedQuantity, $listingId);
        $stmt->execute();
        return $stmt->affected_rows;
    }

    private function getListingImagePath($listingId) {
        $stmt = $this->db->prepare("SELECT image FROM listings WHERE id = ?");
        $stmt->bind_param('i', $listingId);
        $stmt->execute();
        $imageName = $stmt->get_result()->fetch_row()[0];
        $stmt->close();
        return $imageName;
    }

    //remove listing from sale, keeping it in the database (1: success, 0:failure)
    public function removeListing($listingId, $sellerId) {
        $sql = "UPDATE listings SET sellerId = NULL WHERE id = ? AND sellerId = ?";
        $imageName = $this->getListingImagePath($listingId);
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('ii', $listingId, $sellerId);
        $stmt->execute();
        return $stmt->affected_rows;
    }

    private function deleteListing($listingId) {
      $stmt = $this->db->prepare("DELETE FROM listings WHERE id = ?");
      $stmt->bind_param('i', $listingId);
      $result = $stmt->execute();
      $stmt->close();
      return $result;
    }

    //add a listing along with an optional image in the upload directory,
    public function addListing($categoryId, $sellerId, $description, $name, $price, $imageName = NULL) {
        $sql = "INSERT INTO listings(categoryId, sellerId, description, name, price)
                VALUES (?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('iissd', $categoryId, $sellerId, $description, $name, $price);
        $result = $stmt->execute();
        $stmt->close();
        if(!$result) {
            return -1;//insert failed
        }
        if($imageName == NULL) {
            return 0;//insert successful with no image
        }
        $listingId = $this->db->query("SELECT last_insert_id()")->fetch_row()[0];
        $ext = pathinfo($imageName, PATHINFO_EXTENSION);
        $newImageName = "listing_img".$listingId.".".$ext;
        if(file_exists(UPLOAD_DIR.$imageName)) {
            rename(UPLOAD_DIR.$imageName, LISTIMG_DIR.$newImageName);
        } else {
            $this->deleteListing($listingId);
            return -2;//image insertion failed
        }

        $sql = "UPDATE listings
                SET image = ?
                WHERE id = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('si', $newImageName, $listingId);
        $stmt->execute();
        $stmt->close();

        return 0;//insert successful with image
    }

    //add a buyer to the db
    public function addBuyer($mail, $password, $name, $surname, $birthDate, $phone) {
        $sql = "INSERT INTO buyers(mail, password, name, surname, birthDate, phone)
                VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('ssssss', $mail, $password, $name, $surname, $birthDate, $phone);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }

    //add a seller to the db
    public function addSeller($mail, $password, $name, $address, $phone) {
        $sql = "INSERT INTO sellers(mail, password, name, address, phone)
                VALUES (?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('sssss', $mail, $password, $name, $address, $phone);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }

    //get all existing categories
    public function getCategories() {
      $sql = "SELECT * FROM categories";
      $stmt = $this->db->prepare($sql);
      $stmt->execute();
      return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
  }
?>
