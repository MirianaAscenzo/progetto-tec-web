<?php
    //remove listings or add availabilities

    //use variable remId=* to remove a listing

    //use variables id=* and quantity=* to add availability

    require_once './utils/bootstrap.php';

    if(!isset($_SESSION["seller"])) {
        http_response_code(400);
    }

    if(isset($_POST["remId"])) {
        if($dbh->removeListing($_POST["remId"], $_SESSION["seller"]["id"]) == 1) {
            http_response_code(200);
        } else {
            http_response_code(400);
        }
    } else if(isset($_POST["id"]) && isset($_POST["quantity"])) {
        if($dbh->addAvailability($_POST["id"], $_POST["quantity"], $_SESSION["seller"]["id"]) == 1) {
            http_response_code(200);
        } else {
            http_response_code(400);
        }
    } else {
        http_response_code(400);
    }
?>