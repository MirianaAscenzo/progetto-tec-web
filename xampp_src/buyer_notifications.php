<?php
  require_once "./utils/bootstrap.php";

  //check if the session has a user logged in
  checkBuyerLogged();

  //base template params
  $templateParams["css"] = array("base_style.css", "user_base_style.css", "notifications_style.css");
  $templateParams["js"] = array("jquery-1.11.3.min.js", "user_base.js", "buyer_notifications.js");
  $templateParams["title"] = "Grigliatina.it - Notifiche";
  //section template params
  $templateParams["sectionTemplate"] = "buyer_template.php";
  $templateParams["buyer"] = $_SESSION["buyer"];
  $templateParams["nNotifications"] = $dbh->getNBuyerNotifications($_SESSION["buyer"]["id"]);
  //page template params
  $templateParams["pageTemplate"] = "buyer_notifications_page.php";
  $templateParams["notifications"] = $dbh->getBuyerNotifications($_SESSION["buyer"]["id"]);

  require("./templates/base_template.php");
?>
