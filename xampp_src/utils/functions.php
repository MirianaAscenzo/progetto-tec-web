<?php
    function redirect($url, $statusCode = 303)
    {
         header('Location: ' . $url, true, $statusCode);
         die();
    }

    function redirectIfLoggedIn() {
        if(isset($_SESSION["buyer"])) {
            redirect("buyer_home.php");
        }
        if(isset($_SESSION["seller"])) {
            redirect("seller_home.php");
        }
    }

    function checkBuyerLogged() {
        if(!isset($_SESSION["buyer"])) {
            $_SESSION["over"] = true;
            redirect("login.php");
        }
    }

    function checkSellerLogged() {
        if(!isset($_SESSION["seller"])) {
            $_SESSION["over"] = true;
            redirect("login.php");
        }
    }

    function createDirsIfAbsent() {
        if(!file_exists(UPLOAD_DIR)) {
            mkdir(UPLOAD_DIR);
        }
        if(!file_exists(LISTIMG_DIR)) {
            mkdir(LISTIMG_DIR);
        }
    }

    function checkImageUpload(&$errorMessage, &$ext) {
        //check image upload
        $ext = strtolower(pathinfo($_FILES["image"]["name"],PATHINFO_EXTENSION));
        $uploadOk = true;

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check == false) {
            $errorMessage = "Il file non è un'immagine";
            $uploadOk = false;
        }

        // Check file size (max 10MB)
        if ($_FILES["image"]["size"] > 10000000) {
            $errorMessage = "Immagine troppo grande (max 10Mb)";
            $uploadOk = false;
        }

        // Allow certain file formats
        if($ext != "jpg" && $ext != "png" && $ext != "jpeg") {
            $errorMessage = "Formato dell'immagine non supportato";
            $uploadOk = false;
        }
        return $uploadOk;
    }

    function elaborateNewListingRequest(&$errorMessage, $dbh) {        
        $uploadOk = checkImageUpload($errorMessage, $ext);
    
        if(!$uploadOk) {
            return false;
        }
    
        $target_name = "upload".$_SESSION["seller"]["id"].".".$ext;
        $target_file = UPLOAD_DIR.$target_name;
        
        if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
            $errorMessage = "Errore nel caricamento nell'immagine, si prega di riprovare più tardi";
            return false;
        }
    
        $result = $dbh->addListing($_POST["category"], $_SESSION["seller"]["id"], $_POST["description"], $_POST["name"], $_POST["price"], $target_name);
        if($result != 0) {
            $errorMessage = "Errore nella creazione dell'articolo, si prega di riprovare più tardi";
            return false;
        }
        return true;
    }
 ?>
