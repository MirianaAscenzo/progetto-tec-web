<?php
    require_once "./utils/bootstrap.php";

    //check user isn't logged in
    redirectIfLoggedIn();

    //handle create account request
    if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["mail"])) {
        if($_POST["mail"] == "" || $_POST["password"] == "" || $_POST["repeat"] == ""
        || $_POST["phone"] == "" || $_POST["name"] == "" || $_POST["birthDate"] == ""
        || $_POST["surname"] == "") {
            $templateParams["errorMessage"] = "Si prega di riempire i campi vuoti";
        } else if ($_POST["password"] != $_POST["repeat"]) {
            $templateParams["errorMessage"] = "Le password inserite non corrispondono";
        } else {
            switch($dbh->checkUniqueBuyerData($_POST["mail"], $_POST["phone"])) {
                case 0:
                    $dbh->addBuyer($_POST["mail"], $_POST["password"], $_POST["name"], $_POST["surname"], $_POST["birthDate"], $_POST["phone"]);
                    $_SESSION["buyer"] = $dbh->buyerLogin($_POST["mail"], $_POST["password"]);
                    redirect("buyer_home.php");
                    break;
                case 1:
                    $templateParams["errorMessage"] = "La mail è già associata a un altro account";
                    break;
                case 2:
                    $templateParams["errorMessage"] = "Il numero di telefono è già associato a un altro account";
                    break;
            }
        }
        $templateParams["mail"] = $_POST["mail"];
        $templateParams["name"] = $_POST["name"];
        $templateParams["surname"] = $_POST["surname"];
        $templateParams["birthDate"] = $_POST["birthDate"];
        $templateParams["phone"] = $_POST["phone"];
    }

    //base template params
    $templateParams["css"] = array("base_style.css", "login_style.css");
    $templateParams["js"] = array("jquery-1.11.3.min.js", "buyer_sign_up.js", "password_visibility.js");
    $templateParams["title"] = "Grigliatina.it - Registrazione Cliente";
    //section template params
    $templateParams["sectionTemplate"] = "login_template.php";
    //page template params
    $templateParams["pageTemplate"] = "signup_buyer_page.php";

    require("./templates/base_template.php");
 ?>
