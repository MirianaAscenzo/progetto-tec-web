<?php 
    //set as read buyer or seller notification

    //use bId=* for buyer notifications

    //use sId=* for seller notifications

    require_once './utils/bootstrap.php';

    if(isset($_POST["bId"])) {
        if(isset($_SESSION["buyer"]) && $dbh->removeBuyerNotification($_POST["bId"], $_SESSION["buyer"]["id"]) == 1) {
            http_response_code(200);
        } else {
            http_response_code(400);
        }
    } else if(isset($_POST["sId"])) {
        if(isset($_SESSION["seller"]) && $dbh->removeSellerNotification($_POST["sId"], $_SESSION["seller"]["id"]) == 1) {
            http_response_code(200);
        } else {
            http_response_code(400);
        }
    }
?>