<?php
  require_once "./utils/bootstrap.php";

  //check if the session has a user logged in
  checkSellerLogged();

  // TODO: handle listing removal from sale if get variable "id" is present

  //base template params
  $templateParams["css"] = array("base_style.css", "user_base_style.css", "home_style.css");
  $templateParams["js"] = array("jquery-1.11.3.min.js", "user_base.js", "seller_home.js");
  $templateParams["title"] = "Grigliatina.it - Home Venditore";
  //section template params
  $templateParams["sectionTemplate"] = "seller_template.php";
  $templateParams["seller"] = $_SESSION["seller"];
  $templateParams["nNotifications"] = $dbh->getNSellerNotifications($_SESSION["seller"]["id"]);
  //page template params
  $templateParams["pageTemplate"] = "seller_home_page.php";
  $templateParams["listings"] = $dbh->getListings($_SESSION["seller"]["id"]);

  require("./templates/base_template.php");
?>
