$(document).ready(function() {

    $("section article").each(function() {

        let id = $(this).attr("id");

        $(this).find("li > button").click(function ( event ) {
            let quantity = $(this).prev().val();
            $.post("api_managecart.php", {"id": id, "quantity":quantity}, function(data) {
                let response = JSON.parse(data);
                $(event.target).parent().prev().children().first().text(quantity);
                $(event.target).parent().next().children().first().text(response["newprice"] + "€");
                $("section > footer > p").text("Totale: " + response["total"] + "€");
            });
        });

        $(this).find("ul").next().click(function ( event ) {
            $.post("api_managecart.php", {"remId": id}, function(data) {
                $("#"+id).parent().remove();
                $("section > footer > p").text("Totale: " + JSON.parse(data)["total"] + "€");
            });
        });

    });

    $("#payment").click(function(event) {
        if($("#city").val() == ""){
            $("#err").text("Inserire una città");
            return;
        }
        if($("#address").val() == ""){
            $("#err").text("Inserire un indirizzo");
            return;
        }
        if($("#card").val().length > 16 || $("#card").val().length == 0){
            $("#err").text("Inserire un numero di carta valido");
            return;
        }
        if($("#code").val().length > 3 || $("#code").val().length == 0){
            $("#err").text("Inserire un codice di sicurezza valido");
            return;
        }

        let formdata = {"address": $("#address").val() + " " + $("#city").val()}
        $.post("api_payment.php", formdata, function(data){
            message = JSON.parse(data)["errorMessage"];
            if(message === "") {
                location.assign("buyer_orders.php");
            } else if (message == "no items") {
                $("#err").text("Non ci sono articoli nel carrello");
            } else if (message == "items unavailable") {
                location.assign("buyer_shoppingcart.php?showAvail=true#payment");
            } else if (message == "payment error") {
                $("#err").text("C'è stato un errore nell'acquisto, riprovare più tardi");
            }
        })
    })
})