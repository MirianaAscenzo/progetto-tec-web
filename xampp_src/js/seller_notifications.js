$(document).ready(function() {
    $("main > section > article").each(function() {
        
        let id = $(this).attr("id");

        $(this).find("button").click(function (event) {
            $.post("api_notificationread.php", {"sId": id}, function(data) {
                location.reload();
            });
        });

    });
})