function addResultSectionIfAbsent() {
    var sections = $("main > section");
    if(sections.length === 1) {
        const section = `
        <section>
            <header>
                <h3>Risultati</h3>
            </header>
            <div class="container-fluid row" id="results">
            </div>
        </section>`;
        sections.parent().append(section);
    }
}

function getFormData() {
    return {
        "keyword": $("#keyword").val(),
        "min": $("#min").val(),
        "max": $("#max").val(),
        "category": $("#category").val()
    };
}

function showResults(data) {
    console.log(JSON.stringify(data));
    $("#results").empty();
    var listings = JSON.parse(data);
    for(i in listings) {
        let element = `
        <div class="col-md-6">
            <article id="${listings[i]["id"]}">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6 image-column">
                            <img src="${listings[i]["image"]}" alt ="foto di ${listings[i]["name"]}" />
                        </div>
                        <div class="col-sm-6 align-self-center">
                            <ul>
                                <li>${listings[i]["name"]}</li>
                                <li>Prezzo: ${listings[i]["price"]}</li>
                                <li>Categoria: ${listings[i]["categoryName"]}</li>
                            </ul>
                            <button type="button" class="btn btn-dark col-10">Aggiungi al Carrello</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <section>
                                <span>Descrizione</span>
                                <button class="bshow" type="button">Leggi tutto</button>
                                <p>${listings[i]["description"]}</p>
                            </section>
                        </div>
                    </div>
                </div>
            </article>
        </div>`;
        $("#results").append(element);
    }
    $("article section p").hide();
    $("article section button").click(function ( event ) {
        $(this).next().slideToggle("slow");
    })
    $("#results article").each(function() { setAddToCartHandler(this); });
}

function search() {
    addResultSectionIfAbsent();
    let formdata = getFormData();
    $.post("api_search.php", formdata, showResults);
}

$(document).ready(function() {

    $("fieldset").hide();

    $("h4").next().click(function(event) {
        $("fieldset").slideToggle("slow");
    });
    
    $("main > section > div > ul > li:last-child > button")
    .click(search);

    $("main > section > div > ul > li:first-child > input")
    .keypress(function(e) {
        if(e.which == 13) {
            search();
        }
    });

})

