/*sets the handlers for add to function*/
function setAddToCartHandler( selector ) {

    let id = $(selector).attr("id");

    $(selector).find("ul").next().click(function (event) {
        $.post("api_managecart.php", {"addId": id}, function(data) {
            location.assign("buyer_shoppingcart.php");
        });
    });

}

$(document).ready(function() {

    $("aside > ul ul").hide();
    $("aside").hide();

    /*function to toggle aside on and off*/
    $("body > header button").click(function ( event ) {
        $("aside").slideToggle("slow");
    });

    /*function to toggle user information ul on and off*/
    $("aside span").next().click(function ( event ) {
        $("aside > ul ul").slideToggle("slow");
    });

    $("article article").each(function() { setAddToCartHandler(this); });

    $("article section p").hide();

    $("article section button").click(function ( event ) {
        $(this).next().slideToggle("slow");
    })

    $("aside button:last-child").click(function(event) {
        location.replace("logout.php");
    });

})