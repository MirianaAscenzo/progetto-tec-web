$(document).ready(function () {

    $("li").each(function() {

        let thisli = $(this);

        $(this).find("button").click(function (event) {
            
            let input = $(thisli).find("input");

            if ($(input).attr("type") === "password") {
                $(input).attr("type", "text");
            } else {
                $(input).attr("type", "password");
            }
        });
    });

})