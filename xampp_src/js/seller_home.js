$(document).ready(function() {

    $("article").each(function() {

        let id = $(this).attr("id");

        $(this).find("li > button").click(function(event) {
            let quantity = $(this).prev().val();
            $.post("api_managelistings.php", {"id": id, "quantity": quantity}, function(data) {
                span = $(event.target).parent().prev().children().first();
                span.text(parseInt(span.text()) + parseInt(quantity));
            });
        });

        $(this).find("footer > button").click(function(event) {
            $.post("api_managelistings.php", {"remId": id}, function(data) {
                $("#"+id).parent().remove();
            });
        });
    });
})