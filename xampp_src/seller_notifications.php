<?php
  require_once "./utils/bootstrap.php";

  //check if the session has a user logged in
  checkSellerLogged();

  //base template params
  $templateParams["css"] = array("base_style.css", "user_base_style.css", "notifications_style.css");
  $templateParams["js"] = array("jquery-1.11.3.min.js", "user_base.js", "seller_notifications.js");
  $templateParams["title"] = "Grigliatina.it - Notifiche venditore";
  //section template params
  $templateParams["sectionTemplate"] = "seller_template.php";
  $templateParams["seller"] = $_SESSION["seller"];
  $templateParams["nNotifications"] = $dbh->getNSellerNotifications($_SESSION["seller"]["id"]);
  //page template params
  $templateParams["pageTemplate"] = "seller_notifications_page.php";
  $templateParams["notifications"] = $dbh->getSellerNotifications($_SESSION["seller"]["id"]);

  require("./templates/base_template.php");
?>
