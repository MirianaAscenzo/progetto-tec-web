<?php
    require_once "./utils/bootstrap.php";

    //check user isn't logged in
    redirectIfLoggedIn();

    //handle create account request
    if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["mail"])) {
        if($_POST["mail"] == "" || $_POST["password"] == "" || $_POST["repeat"] == ""
        || $_POST["phone"] == "" || $_POST["name"] == "") {
            $templateParams["errorMessage"] = "Si prega di riempire i campi vuoti";
        } else if ($_POST["password"] != $_POST["repeat"]) {
            $templateParams["errorMessage"] = "Le password inserite non corrispondono";
        } else {
            switch($dbh->checkUniqueSellerData($_POST["mail"], $_POST["phone"])) {
                case 0:
                    $dbh->addSeller($_POST["mail"], $_POST["password"], $_POST["name"], $_POST["address"], $_POST["phone"]);
                    $_SESSION["seller"] = $dbh->sellerLogin($_POST["mail"], $_POST["password"]);
                    redirect("seller_home.php");
                    break;
                case 1:
                    $templateParams["errorMessage"] = "La mail è già associata a un altro account";
                    break;
                case 2:
                    $templateParams["errorMessage"] = "Il numero di telefono è già associato a un altro account";
                    break;
            }
        }
        $templateParams["mail"] = $_POST["mail"];
        $templateParams["name"] = $_POST["name"];
        $templateParams["phone"] = $_POST["phone"];
    }

    //base template params
    $templateParams["css"] = array("base_style.css", "login_style.css");
    $templateParams["js"] = array("jquery-1.11.3.min.js", "password_visibility.js");
    $templateParams["title"] = "Grigliatina.it - Registrazione Venditore";
    //section template params
    $templateParams["sectionTemplate"] = "login_template.php";
    //page template params
    $templateParams["pageTemplate"] = "signup_seller_page.php";

    require("./templates/base_template.php");
?>
