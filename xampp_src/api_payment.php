<?php
    //try payment, on success (availabilities ok), the response is 200 (success)
    //on failure the response is 406

    //needs address as a variable

    //return 200 on ok and 400+ on error along with errorMessage

    require_once './utils/bootstrap.php';

    //$_SESSION["buyer"] = array("id" => 1);

    switch($dbh->purchaseShoppingCartListings($_SESSION["buyer"]["id"], $_POST["address"])) {
        case 0:
            http_response_code(200);
            $message = "";
            break;
        case -1:
            http_response_code(200);
            $message = "items unavailable";
            break;
        case -2:
            http_response_code(200);
            $message = "no items";
            break;
        case -3:
            http_response_code(200);
            $message = "payment error";
            break;
    }

    echo json_encode(array("errorMessage" => $message));
?>