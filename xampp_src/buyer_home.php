<?php
  require_once "./utils/bootstrap.php";

  //check if the session has a user logged in
  checkBuyerLogged();

  // TODO: handle add to shoppin cart (with redirect) the get variable "id" is present

  //base template params
  $templateParams["css"] = array("base_style.css", "user_base_style.css", "home_style.css", "buyer_home_style.css");
  $templateParams["js"] = array("jquery-1.11.3.min.js", "user_base.js");
  $templateParams["title"] = "Grigliatina.it - Home Cliente";
  //section template params
  $templateParams["sectionTemplate"] = "buyer_template.php";
  $templateParams["buyer"] = $_SESSION["buyer"];
  $templateParams["nNotifications"] = $dbh->getNBuyerNotifications($_SESSION["buyer"]["id"]);
  //page template params
  $templateParams["pageTemplate"] = "buyer_home_page.php";
  $templateParams["home_categories"] = $dbh->homepageListings();
  if(count($templateParams["home_categories"]) > 0) {
      $templateParams["category"] = array_keys($templateParams["home_categories"])[0];
  }
  

  require("./templates/base_template.php");
?>
