<?php
  require_once "./utils/bootstrap.php";

  //check if the session has a user logged in
  checkBuyerLogged();

  $listings = $dbh->getShoppingCartListings($_SESSION["buyer"]["id"]);
  $total = 0;
  foreach ($listings as $listing) {
      $total = $total + ($listing["quantity"] * $listing["price"]);
  }
  if(isset($_GET["showAvail"])) {
      foreach ($listings as &$listing) {
          if($listing["quantity"] > $listing["nAvailable"]) {
              $listing["errorMessage"] = "Il prodotto non è disponibile nella quantità selezionata (".$listing["nAvailable"]." disponibili)";
          }
      }
      $templateParams["errorMessage"] = "Alcuni articoli selezionati non sono disponibili nella quantità richiesta";
  }

  //base template params
  $templateParams["css"] = array("base_style.css", "user_base_style.css", "background_style.css", "error_style.css", "shopping_cart_style.css");
  $templateParams["js"] = array("jquery-1.11.3.min.js", "user_base.js", "shopping_cart.js");
  $templateParams["title"] = "Grigliatina.it - Carrello";
  //section template params
  $templateParams["sectionTemplate"] = "buyer_template.php";
  $templateParams["buyer"] = $_SESSION["buyer"];
  $templateParams["nNotifications"] = $dbh->getNBuyerNotifications($_SESSION["buyer"]["id"]);
  //page template params
  $templateParams["pageTemplate"] = "buyer_shoppingcart_page.php";
  $templateParams["listings"] = $listings;
  $templateParams["total"] = $total;

  require("./templates/base_template.php");
?>
