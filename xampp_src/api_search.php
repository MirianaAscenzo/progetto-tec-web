<?php
    //get a json of listings

    //uses the form variables

    require_once './utils/bootstrap.php';

    $min = 0;
    $max = 0;

    if($_POST["min"] != "") {
        $min = $_POST["min"];
    }

    if($_POST["max"] != "") {
        $max = $_POST["max"];
    }

    if($_POST["category"] == 'all') {
        $listings = $dbh->searchListings($_POST["keyword"], $min, $max);
    } else {
        $listings = $dbh->searchListingsByCategory($_POST["keyword"], $_POST["category"] ,$min, $max);
    }

    foreach ($listings as &$listing) {
        $listing["image"] = LISTIMG_DIR.$listing["image"];
        $response[$listing["id"]] = $listing;
    }

    http_response_code(200);
    echo json_encode($response);
?>