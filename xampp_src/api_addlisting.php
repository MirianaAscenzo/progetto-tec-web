<?php
    //add a listing to the seller's list

    //uses the form's variables 

    //responses: 200 ok, 400 bad request (no image or bad image)
    //data response: errormessage (to consider only on failure)

    require_once './utils/bootstrap.php';

    $errorMessage = ""; 

    if(!isset($_POST["submit"])){
        http_response_code(400);
        echo json_encode(array('errormessage' => $errorMessage));
        return;
    }
    
    $uploadOk = checkImageUpload($errorMessage, $ext);

    if(!$uploadOk) {
        http_response_code(400);
        echo json_encode(array('errormessage' => $errorMessage));
        return;
    }

    $target_name = "upload".$_SESSION["seller"]["id"].".".$ext;
    $target_file = UPLOAD_DIR.$target_name;
    
    if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
        http_response_code(400);
        $errorMessage = "Errore nel caricamento nell'immagine, si prega di riprovare più tardi";
        echo json_encode(array('errormessage' => $errorMessage));
        return;
    }

    $result = $dbh->addListing($_POST["category"], $_SESSION["seller"]["id"], $_POST["description"], $_POST["name"], $_POST["price"], $target_name);
    if($result != 0) {
        http_response_code(400);
        $errorMessage = "Errore nella creazione dell'articolo, si prega di riprovare più tardi";
        echo json_encode(array('errormessage' => $errorMessage));
        echo $result;
        return;
    }

    http_response_code(200);
?>